variable "cred_file" {
  type = string
  default = "k8s-practice-326211-198d1f19028d.json"
}
variable "project_id" {
  type = string
  default = "k8s-practice-326211"
}
