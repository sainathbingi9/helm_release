# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/google" {
  version = "3.86.0"
  hashes = [
    "h1:i227DCKQ4Zp9MdZeVzrSfweAlaP4AOv8EKpXSkgwWRU=",
    "zh:06531f545e6fc5338599886438497b0c5b54de08a05e2b8d89e19b2acca2586d",
    "zh:1425efb842902a21389d8c397db1b97c6fce196919ec960594fc713fff9fda30",
    "zh:1a46aebe284d713cba984d3ec7c26203ad1ae6b99685a526d336faddd6eb5d9e",
    "zh:1e90c96273400a7c6eedef4258c07fbcfa62c5de6c83a2063cd60db13a6f51f5",
    "zh:453abe965674d5a79e59ab8e1d3a6978ea8decfe8240610f54719c85201e8ac4",
    "zh:78aac8e55d9edb623d29e734d43e096cc09f07cddee69a6f2eb0021fc24358b3",
    "zh:a56c9cb2f9d2f4790e9d06996b385b0f61a7dc2418174fa290dcfc911390c54b",
    "zh:cd45f335dd94734ef5bd87a3359691131fe734d324bbc0d001f96a1da456fcb0",
    "zh:d4094edd5f893103f61b0badffb0ae133540fc3fc57557515ebf983494a0c970",
    "zh:e1f4c3631d47456869af90d4924872c13569ea0f963481ea3b9594b1b087fb12",
    "zh:ea2a2e9fd75a3e14e524f4e2b30d07e700e1f0558b3bc2c8497658bc556d8053",
  ]
}

provider "registry.terraform.io/hashicorp/helm" {
  version = "2.3.0"
  hashes = [
    "h1:K4P1DLu2Ne8jICr9LUr5RIaUuu6Q6EaWJwW2EyudNs4=",
    "zh:0c63c0aa9f13ec057971c9ddd0965e062b064c0b2ce9a3d13df41239b8f5af89",
    "zh:14b4146e56b1c1e5492ce597d03fb993a71039dd701e3886667f7ad3d60afd34",
    "zh:3882a3a091aeac1d42a01c7654c727a46870185d363454579e707c672a42f430",
    "zh:3ccb84b23348e2b64ffe273170f686e6927e974060e37b52459ab2395bd59523",
    "zh:4fb709aeb63dd6dee1e851459ac87c399ddb831ebfe9e28e4c9827c65bb55b67",
    "zh:6739a4da9510f552b93fa77859f04c20af3a7a40a7f0b0073eb723af36b8156b",
    "zh:7432b070124a161fe6a32dc6b76fd45100d079fdee917318460a82b80cda5518",
    "zh:9aa51dc2a649fcaac133dad5c1e148c2e0f675fdf18b1d3b1da53549d8fa3b36",
    "zh:a9a2abf1fbec56019473c8696d285703218ea0e8f2dc01879e7a391ea8785d57",
    "zh:b3c25b085b7a7fbc05000303d82c54531987525575ff337e4be6fc152f4200d4",
    "zh:fa8cb34df7786bbeffcee664eab72aeb15c4f8fba2d9c0b05faca6b3e819cf33",
  ]
}
