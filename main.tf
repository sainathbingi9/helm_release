provider "google" {
  credentials = "${file(var.cred_file)}"
  project = var.project_id
}
data "google_client_config" "provider" {}
data "google_container_cluster" "ci_cd_cluster2" {
  name = "ci-cd-cluster2"
  location = "us-east1"
}
provider "helm" {
  kubernetes {
    token = data.google_client_config.provider.access_token
    host  = "https://${data.google_container_cluster.ci_cd_cluster2.endpoint}"
    cluster_ca_certificate = base64decode(
    data.google_container_cluster.ci_cd_cluster2.master_auth[0].cluster_ca_certificate,
    )
  }
}

resource "helm_release" "nginx" {
  name       = "nginx"

  repository = "https://charts.bitnami.com/bitnami"
  chart      = "nginx"
}